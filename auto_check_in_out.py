#!/usr/bin/env python

import time
import random
import os
import sys


DEBUG=0
#DEBUG=1


def debug_print(x):
    if DEBUG == 1:
        print(x)
        sys.stdout.flush()


def sesametime_update(user, password, status):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    os.system(dir_path+"/update_sesametime.sh {} {} \"{}\"".format(user, password, status))



lt                = time.localtime()
current_timestamp = lt.tm_hour*60 + lt.tm_min


# Obtain what the status of each user should be
#
user_password       = {}
user_desired_status = {}

for l in open("users.txt").readlines():

    l = l.strip()

    # Ignore comments
    #
    if l.startswith("#"):
        continue

    tokens = l.split(",")

    # Ignore invalid/empty lines
    #
    if len(tokens) != 3:
        continue

    user, passwd, slots = tokens


    # Save password for later
    #
    user_password[user] = passwd


    # Find if current time matches a user slot
    #
    user_desired_status[user] = "check out"

    for slot in slots.split("&"):
        ci, co = slot.split("-")
        
        ci_timestamp = int(ci.split(":")[0])*60 + int(ci.split(":")[1])
        co_timestamp = int(co.split(":")[0])*60 + int(co.split(":")[1])

        if abs(current_timestamp - ci_timestamp) <= 10:
            user_desired_status[user] = "check in"
            break

        elif abs(current_timestamp - co_timestamp) <= 10:
            user_desired_status[user] = "check out"
            break

        elif current_timestamp > ci_timestamp and \
             current_timestamp < co_timestamp:
            user_desired_status[user] = "check in"
            break


debug_print("User desired status:")
debug_print(user_desired_status)
debug_print("")


# For each user, randomly choose a time (in the next 10 minutes) to update its
# status
#
random.seed()
users = []

for user in user_desired_status.keys():
    users.append((random.randint(0,9*60), user))

users.sort()

debug_print("On which second (from now) should we update each user")
for x in users:
    debug_print("{:>4} {}".format(x[0], x[1]))
debug_print("")


# Sleep, wake up and then check and update the status of each user
#
current = 0
for x in users:
    next_wake_up = x[0] - current

    debug_print("")
    debug_print("Sleeping for {} seconds...".format(next_wake_up))
    time.sleep(next_wake_up)

    debug_print("Processing user {} ({})".format(x[1], user_desired_status[x[1]]))
    sesametime_update(x[1], user_password[x[1]], user_desired_status[x[1]])

    current += next_wake_up


