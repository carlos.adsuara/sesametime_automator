#!/bin/bash

# Usage:
#
#   ./update_sesametime.sh <username> <password> <status>
#
# ...where:
#
#   <username> is the sesametime login username (ex: "john@moon.com")
#   <password> is the sesametime login password
#   <status>   is the desired status. Can be "check in" or "check out"


USER=`echo $1 | sed 's:@:%40:g'`
PASSWORD=$2
STATUS=$3

#echo $USER $PASSWORD $STATUS
#exit 0


resolve_redirect() {

        REDIRECT=$1

        while true; do
                curl $REDIRECT \
                     -I \
                     -H "Cookie: $COOKIE1; $COOKIE2" \
                     -s \
                     > _tmp_headers.txt

                if cat _tmp_headers.txt | grep ocation: >/dev/null; then
                        REDIRECT=`cat _tmp_headers.txt | grep ocation: | awk '{print $2}' | tr --delete '\n' | tr --delete '\r'`
                        echo "INFO: Redirect --> $REDIRECT"
                else
                        break
                fi
        done
}


sesametime_login() {
        # Login and obtain cookies
        #
        curl "https://panel.sesametime.com/" \
             --data \
             "_method=POST&data%5BUser%5D%5Bemail%5D=${USER}&data%5BUser%5D%5Bpassword%5D=${PASSWORD}" \
             -c _tmp_cookies.txt
        
        COOKIE1=`cat _tmp_cookies.txt | grep CakeCookie | awk '{print $6"="$7}'`
        COOKIE2=`cat _tmp_cookies.txt | grep CAKEPHP    | awk '{print $6"="$7}'`
}


sesametime_status() {
        # Get current status
        #
        resolve_redirect "https://panel.sesametime.com/admin/users/checks"

        curl $REDIRECT \
             -H "Cookie: $COOKIE1; $COOKIE2" \
             -s \
             > _tmp_status.txt
        
        if cat _tmp_status.txt | grep "Check OUT" >/dev/null; then
                CURRENT_STATUS="check in"
        
        elif cat _tmp_status.txt | grep "Check IN" >/dev/null; then
                CURRENT_STATUS="check out"
        
        else
                CURRENT_STATUS="error"
                echo "ERROR: Could not check status"
                exit -1
        fi
}


sesametime_check_if_today_is_holiday() {

        resolve_redirect "https://panel.sesametime.com/admin/vacations/view"
        
        curl $REDIRECT \
             -H "Cookie: $COOKIE1; $COOKIE2" \
             -s \
             > _tmp_holiday.txt

        CELL_COLOR=$(cat _tmp_holiday.txt | grep -o "data-maincolor[^<>]*"`date +%Y-%m-%d` | awk '{print $1}' | cut -d= -f2)
        #CELL_COLOR=$(cat _tmp_holiday.txt | grep -o "data-maincolor[^<>]*"2019-11-04 | awk '{print $1}' | cut -d= -f2)

        if [ "$CELL_COLOR" == "\"\"" ]; then
                HOLIDAY=0
        else
                HOLIDAY=1
        fi
}


sesametime_change_status() {
        # Change from check in to check out and viceversa
        #
        resolve_redirect "https://panel.sesametime.com/admin/checks/check_panel"

        curl $REDIRECT \
             -H "Cookie: $COOKIE1; $COOKIE2" \
             -s \
             > /dev/null
}



################################################################################
# main()
################################################################################

# Get session cookies
#
sesametime_login


# Check if today is a holiday
#
sesametime_check_if_today_is_holiday

if [ "$HOLIDAY" == "1" ] && [ "$STATUS" == "check in" ]; then
        echo "INFO: Today is a holiday. I won't check in."
        exit 0
fi


# Check current status
#
sesametime_status


# If needed, update status
#
if ! [ "$STATUS" == "$CURRENT_STATUS" ]; then
        echo "INFO: Changing status to $STATUS"
        sesametime_change_status
else
        echo "INFO: Current status is already the desired one. No need to change anything"
        exit 0
fi


# Re-check status
#

sleep 2
sesametime_status

if ! [ "$STATUS" == "$CURRENT_STATUS" ]; then
        echo "ERROR: Could not change status"
        exit -1

fi


# Everything ok
#
exit 0



