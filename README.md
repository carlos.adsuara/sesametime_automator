# Description

"sesametime.com" is a website that some companies use so that employees can keep
track of their working hours and holidays.

The "problem" is that "sesametime.com" "forces" you to manually indicate when
you are starting to work and when you are stopping to take a break or to return
home.

This repository contains a set of scripts and a set of systemd files that are
meant to be "installed" on an "always on" PC so that it can perform the auto
check in and out to/from the "sesametime.com" website at predefined times.

  NOTE: I know this automation defies the purpose of "sesametime.com", but I
        also value my time and sanity too much to have to manually interact
        with yet another website at work *every day*, *several times per day*.


# Install

This is what you have to do:

  1. On the "always on" PC, create a new folder (ex:
     "/home/developer/src/sesametime") and copy there these files: 

       - auto_check_in_out.py
       - update_sesame_time.sh 

  2. Give execution permissions to those files

  3. On that same folder, create a file called "users.txt" that follows the
     same format as the included example file. That is:

       - One line per user
       - Each line contains 3 fields separated by ",":
           - The first field is the "sesametime.com" username
           - The second field is the "sesametime.com" password
           - The third field is a list of "check in" time slots separated by "&"
             where you want to be "checked in" (ie. "working")

  4. Copy these files to "/etc/systemd/system" on the "always on" PC:

       - systemd/auto_check_in_out.service
       - systemd/auto_check_in_out.timer

  5. Edit the just copied file "systemd/auto_check_in_out.service" so that
     in contains the actual path to the folder you created in (step 1)

  6. On the "always on" PC, run these commands:

       $ systemctl enable auto_check_in_out.timer
       $ systemctl start  auto_check_in_out.timer

  7. That's all! The PC will automatically take care of checking all users in
     and out according to the time slots defined in file "users.txt".
     Also, even if the PC reboots, because you have installed the script as
     a systemd service, it will keep working


# Uninstall

In order to uninstall eveything, do this:

  1. Run this on the "always on" PC:

       $ systemctl stop    auto_check_in_out.timer
       $ systemctl disable auto_check_in_out.timer

  2. Delete these files:

       - /etc/systemd/system/auto_check_in_out.service
       - /etc/systemd/system/auto_check_in_out.timer

  3. Delete the folder (and the contents) where you originally placed files
     "auto_check_in_out.py", "update_sesame_time.sh" and "users.txt"


# How it works

When you enable/start the "auto_check_in_out.timer", systemd is configured to
run the "auto_check_in_out.service" service at certain times (as defined inside
the "auto_check_in_out.timer", which by default is at all xx:20 and xx:50
minutes from 7am to 22pm).

This "auto_check_in_out.service" runs the "auto_check_in_out.py" script, which
parses "users.txt" to figure out what the status of each user should be *right
now*, at the time the script is run (ex: "user1 should be checked in", "user2
should also be checked in", "user3 should be checked out", ...).

Then, for each user, script "update_sesametime.sh" is run with three arguments:
the username, the password and the status.
This script then logs into "sesametime.com", checks the user current state and,
if it doesn't match the provided one, it changes it.
Also, before doing any of this, the script also checks with "sesametime.com" if
the user is on holidays and, if so, it doesn't do anything.



